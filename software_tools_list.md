# List of software tools needed:
- ???
R
Spades
Abyss
fastQC
MultiQC
Longqc
Flye
Pilon
Mummer
Samtools
Card 
Maker
minimap2
seqkit
get_organelle
assembly-stats
seqtk
GeSeq
# https://chlorobox.mpimp-golm.mpg.de/geseq.html
RGI
Quast
pgap    https://www.ncbi.nlm.nih.gov/genome/annotation_prok/
	https://github.com/ncbi/pgap
Fmlrc  https://github.com/holtjma/fmlrc
redundans
recon
kmer genie http://kmergenie.bx.psu.edu/
